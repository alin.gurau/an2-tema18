<?php
    class train {
        public $type;
        public $wagon;

    public function setType($type)
    {
        $this->type=$type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setWagon($wagon)
    {
        $this->wagon=$wagon;
    }

    public function getWagon()
    {
        return $this->wagon;
    }
}

$regionalTrain = new train();
$regionalTrain->setType('diesel');
$regionalTrain->setWagon(8);

$interstateTrain = new Train();
$interstateTrain->setType('electric');
$interstateTrain->setWagon(4);
?>