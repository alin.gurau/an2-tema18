<?php
    class plane {
        public $name;
        public $type;
        public $seats;

        public function __construct($name, $type, $seats)
        {
            $this->name=$name;
            $this->type=$type;
            $this->seats=$seats;
        }

        public function setName($name)
        {
            $this->name=$name;
        }

        public function getName()
        {
            return $this->name;
        }

        public function setType($type)
        {
            $this->type=$type;
        }

        public function getType()
        {
            return $this->type;
        }

        public function setSeats($seats)
        {
            $this->seats=$seats;
        }

        public function getSeats()
        {
            return $this->seats;
        }

    public function attack()
    {
        return ' engaged in attack';
    }

    public function takeOff()
    {
        return ' is ready to take off!';
    }

    public function land()
    {
        return ' just landed.';
    }
    }

    $plane = new plane('Wright Flyer', 'airplane', 2);

    $plane2 = new plane('Soundwave', 'aircraft', 1);
    $plane2->setName('Supermarine Spitfire');

   
?>