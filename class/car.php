<?php
    class car extends vehicle {
        public $doors;

        public function __construct($model)
        {
            parent::setModel($model);
            parent::setWheels(4);
        }

        public function setDoors($doors)
        {
            $this->doors = $doors;
        }

        public function getDoors() 
        {
            return $this->doors;
        }
 
        public function start() {
            return 'Engine started';
        }
    }

    $sportCar = new Car('Ferrari');
    $sportCar->setWheels(4);
    $sportCar->setDoors(2);
?>