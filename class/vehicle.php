<?php
    class vehicle {
    public $model;
    public $wheels;

    public function __construct($model)
    {
        $this->model = $model;
        $this->wheels = 4;
    }
 
    public function setModel($model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return $this->model;
    }

    public function setWheels($wheels)
    {
        $this->wheels = $wheels;
    }

    public function getWheels()
    {
        return $this->wheels;
    }

    public function hello()
    {
        return "I am a <i>" . $this->model . "</i><br />";
    }

    public function displayWheels()
    {
        return "The " . $this->model . ' has ' . $this->wheels . ' wheels.';
    }   
}
?>